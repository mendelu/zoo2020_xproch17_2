//
// Created by David on 09/11/2020.
//

#include "Sklad.h"

Sklad::Sklad(int pocetPater){
    /*
    std::vector<Patro*> patra(pocetPater, new Patro());
    m_patra = patra;
    */

    for (int i = 0; i < pocetPater; ++i) {
        Patro* novePatro = new Patro();
        m_patra.push_back(novePatro);
    }
}

Sklad::~Sklad(){
    for (int i = 0; i < m_patra.size(); ++i) {
        delete m_patra.at(i);
    }
}

void Sklad::ulozKontejner(Kontejner* kontejner, int patro, int pozice){
    if ((patro >= 0) and (patro < m_patra.size())){
        m_patra.at(patro)->ulozKontejner(kontejner, pozice);
    } else {
        std::cout << "Sklad:ulozKontejner: Patro musi mit index od 0 do "
            << m_patra.size()-1 << std::endl;
    }
    /*
    Patro* aktivniPatro = m_patra.at(patro);
    aktivniPatro->ulozKontejner(kontejner, pozice);
    */

}

void Sklad::odeberKontejner(int patro, int pozice){
    if ((patro >= 0) and (patro < m_patra.size())){
        m_patra.at(patro)->odeberKontejner(pozice);
    } else {
        std::cout << "Sklad:odeberKontejner: Patro musi mit index od 0 do "
                  << m_patra.size()-1 << std::endl;
    }
}

void Sklad::vypisObsahSkladu(){
    for (int i = 0; i < m_patra.size(); ++i) {
        std::cout << "Patro c. " << i << std::endl;
        m_patra.at(i)->vypisObsahPatra();
    }
}