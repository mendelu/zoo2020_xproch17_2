//
// Created by David on 09/11/2020.
//

#ifndef CVICENI_8_SKLAD_H
#define CVICENI_8_SKLAD_H

#include <vector>
#include "Patro.h"

class Sklad {
    std::vector<Patro*> m_patra;
public:
    Sklad(int pocetPater);
    void ulozKontejner(Kontejner* kontejner, int patro, int pozice);
    void odeberKontejner(int patro, int pozice);
    void vypisObsahSkladu();
    ~Sklad();
};


#endif //CVICENI_8_SKLAD_H
