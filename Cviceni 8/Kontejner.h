//
// Created by David on 09/11/2020.
//

#ifndef CVICENI_8_KONTEJNER_H
#define CVICENI_8_KONTEJNER_H

#include <iostream>

class Kontejner {
    std::string m_popis;
public:
    Kontejner(std::string popis);
    void vypisObsah();
};


#endif //CVICENI_8_KONTEJNER_H
