//
// Created by David on 09/11/2020.
//

#ifndef CVICENI_8_PATRO_H
#define CVICENI_8_PATRO_H

#include <array>
#include "Kontejner.h"

class Patro {
    std::array<Kontejner*, 10> m_pozice;
public:
    Patro();
    void ulozKontejner(Kontejner* kontejner, int pozice);
    void odeberKontejner(int pozice);
    void vypisObsahPatra();
};


#endif //CVICENI_8_PATRO_H
