//
// Created by David on 09/11/2020.
//

#include "Patro.h"

Patro::Patro(){
    for(Kontejner* &pozice:m_pozice){
        pozice = nullptr;
    }
    /*
    for(int i=0; i<m_pozice.size(); ++i){
        m_pozice.at(i) = nullptr;
    }
     */
}

void Patro::ulozKontejner(Kontejner* kontejner, int pozice){
    if ((pozice >= 0) and (pozice < m_pozice.size())) {
        if (m_pozice.at(pozice) == nullptr) {
            m_pozice.at(pozice) = kontejner;
        } else {
            std::cout << "Pozice " << pozice << " je obsazena" << std::endl;
        }

    } else {
        std::cout << "Patro::ulozKontejner: Pozice musi byt 0 az " << m_pozice.size()-1 << std::endl;
    }
}

void Patro::odeberKontejner(int pozice){
    if ((pozice >= 0) and (pozice < m_pozice.size())) {
        if (m_pozice.at(pozice) != nullptr) {
            m_pozice.at(pozice) = nullptr;
        } else {
            std::cout << "Pozice " << pozice << " je volna" << std::endl;
        }
    } else {
        std::cout << "Patro::odeberKontejner: Pozice musi byt 0 az " << m_pozice.size()-1 << std::endl;
    }
}

void Patro::vypisObsahPatra(){
    for(int i=0; i<m_pozice.size(); ++i){
        std::cout << "Pozice " << i << ": ";
        m_pozice.at(i)->vypisObsah();
        std::cout << std::endl;
    }
}