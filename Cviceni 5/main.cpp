/**
 * IS školy: Evidujeme studenty a kurzy. Každý student má jméno,
 * rok narození, bydliště, které jsou povinné. Zadané hodnoty
 * nesmí být prázdné. Pokud bydliště studenta neznáme, musíme
 * zadat text "Bydliště neznámé". Kurz má název a cenu v Kč.
 * Obě informace jsou povinné.
 *
 * Studenti mohou navštěvovat libovolné množství kurzů a každý kurz
 * má více studentů. Evidujme v IS známku daného studenta v daném
 * kurzu. Známka je studentovi přiřazena i později. Ne hned po
 * zapsání do kurzu.
 *
 * Vytvořte jeden kurz, jednoho studenta a dejte mu známku v kurzu.
 * Vypište na obrazovku jméno studenta, kurzu a jeho známku pomocí
 * jedné metody.
 */

#include <iostream>
using namespace std;

class Student{
    string m_jmeno;
    string m_bydliste;
    int m_rokNarozeni;
public:
    Student(string jmeno, string bydliste, int rokNarozeni){
        setJmeno(jmeno);
        setBydliste(bydliste);
        m_rokNarozeni = rokNarozeni;
    }

    Student(string jmeno, int rokNarozeni):Student(jmeno, "Není známo", rokNarozeni){
    }

    string getJmeno(){
        return m_jmeno;
    }
private:
    void setJmeno(string jmeno){
        if (jmeno != ""){
            m_jmeno = jmeno;
        } else {
            cout << "Student: jmeno nemuze byt prazdne" << endl;
            m_jmeno = "Není známo";
        }
    }

    void setBydliste(string bydliste){
        if (bydliste != ""){
            m_bydliste = bydliste;
        } else {
            cout << "Student: bydliste nemuze byt prazdne" << endl;
            m_bydliste = "Není známo";
        }
    }
};

class Kurz{
    string m_nazev;
    int m_cena;
public:
    Kurz(string nazev, int cena){
        setNazev(nazev);
        m_cena = cena;
    }

    string getNazev(){
        return m_nazev;
    }

private:
    void setNazev(string nazev){
        if (nazev != ""){
            m_nazev = nazev;
        } else {
            cout << "Kurz: nazev nemuze byt prazdny" << endl;
            nazev = "Není znám";
        }
    }
};

class Zapis{
    int m_znamka;
    Student* m_zapsanyStudent;
    Kurz* m_zapsanyKurz;
public:
    Zapis(Kurz* kurz, Student* student){
        setKurz(kurz);
        setStudent(student);
        m_znamka = 5;
    }

    void setZnamka(int znamka){
        if ((znamka > 5) or (znamka < 1)){
            cout << "Zapis: znamka musi byt na intervalu <1,5>";
        } else {
            m_znamka = znamka;
        }
    }

    void printInfo(){
        cout << "Znamka: " << m_znamka << endl;
        if (m_zapsanyStudent != nullptr) {
            cout << "Jmeno studenta: " << m_zapsanyStudent->getJmeno() << endl;
        }
        if (m_zapsanyKurz != nullptr) {
            cout << "Nazev kurzu: " << m_zapsanyKurz->getNazev() << endl;
        }
    }

private:
    void setKurz(Kurz* kurz){
        if (kurz != nullptr){
            m_zapsanyKurz = kurz;
        } else {
            cout << "Zapis: nelze ukladat prazdny kurz" << endl;
            m_zapsanyKurz = nullptr;
        }
    }

    void setStudent(Student* student){
        if (student != nullptr){
            m_zapsanyStudent = student;
        } else {
            cout << "Zapis: nelze ukladat prazdneho studenta" << endl;
            m_zapsanyStudent = nullptr;
        }
    }
};


int main() {
    Student* david = new Student("David", "Brno", 1990);
    Kurz* anglictina = new Kurz("Zaklady AJ", 1000);
    Zapis* davidDoAj = new Zapis(nullptr, david);
    //anglictina->setNazev("Zaklady anglickeho jazyka");
    davidDoAj->printInfo();

    delete davidDoAj;
    delete anglictina;
    delete david;
    return 0;
}
