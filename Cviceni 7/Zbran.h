//
// Created by David on 02/11/2020.
//

#ifndef CVICENI_7_ZBRAN_H
#define CVICENI_7_ZBRAN_H


class Zbran {
    float m_vaha;
    int m_bonusUtoku;
public:
    Zbran(float vaha, int bonusUtoku);
    int getBonusUtoku();
    float getVaha();
};


#endif //CVICENI_7_ZBRAN_H
