//
// Created by David on 02/11/2020.
//

#ifndef CVICENI_7_BRNENI_H
#define CVICENI_7_BRNENI_H


class Brneni {
    int m_bonusObrany;
    float m_vaha;
public:
    Brneni(int bonusObrany, float vaha);
    int getBonusObrany();
    float getVaha();
};


#endif //CVICENI_7_BRNENI_H
