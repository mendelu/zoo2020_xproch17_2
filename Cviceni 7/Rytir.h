//
// Created by David on 02/11/2020.
//

#ifndef CVICENI_7_RYTIR_H
#define CVICENI_7_RYTIR_H

#include <vector>
#include "Zbran.h"
#include "Brneni.h"
#include "Napoj.h"

class Rytir {
    int m_zivot;
    int m_utok;
    int m_obrana;
    Zbran* m_zbran;
    Brneni* m_brneni;
    std::vector<Napoj*> m_napoje;
public:
    Rytir(int zivot, int utok, int obrana, Zbran* zbran, Brneni* brneni);
    void seberNapoj(Napoj* napoj);
    void vypijPosledniNapoj();
    void zahodPosledniNapoj();
    void printInfo();
    //void odectiZdravi(int kolik);
};


#endif //CVICENI_7_RYTIR_H
