//
// Created by David on 02/11/2020.
//

#include "Rytir.h"

Rytir::Rytir(int zivot, int utok, int obrana, Zbran* zbran, Brneni* brneni){
    m_zivot = zivot;
    m_utok = utok;
    m_obrana = obrana;
    m_zbran = zbran;
    m_brneni = brneni;
}

void Rytir::seberNapoj(Napoj* napoj){
    m_napoje.push_back(napoj);
}

void Rytir::vypijPosledniNapoj(){
    if (m_napoje.size() > 0){
        m_zivot += m_napoje.at(m_napoje.size()-1)->getBonusZdravi();
        m_napoje.pop_back(); // zahozeni
    }
}

void Rytir::zahodPosledniNapoj(){
    if (m_napoje.size() > 0){
        m_napoje.pop_back(); // zahozeni
    }
}

void Rytir::printInfo(){

}
