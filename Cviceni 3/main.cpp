#include <iostream>
using namespace std;

class Auto{
    float m_najetoKm;
    float m_cenaZaDen;
    float m_vydelaloKc;
public:
    Auto(float najetoKm, float cenaZaDen, float vydelaloKc){
        m_cenaZaDen = cenaZaDen;
        m_najetoKm = najetoKm;
        m_vydelaloKc = vydelaloKc;
    }

    float getPujcovne(float pocetDni){
        return pocetDni*m_cenaZaDen;
    }

    void evidujZapujcku(float ujetoKm, float pocetDni){
        m_najetoKm += ujetoKm;
        m_vydelaloKc += getPujcovne(pocetDni);
        upravPujcovne(ujetoKm);
    }

    void printInfo(){
        cout << "Najeto: " << m_najetoKm << endl;
        cout << "Cena za den: " << m_cenaZaDen << endl;
        cout << "Vydelano Kc: " << m_vydelaloKc << endl;
    }
private:
    void upravPujcovne(float ujetoKm){
        // presahli jsme deset tisic km?
        // kolik je deset procent
        // m_cenaZaDen -= desetProcentCeny;
    }
};

int main() {
    Auto* ford = new Auto(10000, 500, 0);
    ford->evidujZapujcku(1000, 3);
    ford->printInfo();
    //ford->upravPujcovne(100);
    delete ford;
    return 0;
}
